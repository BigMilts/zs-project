import React, { useState, useEffect } from 'react';

import {
  ArticleContainer,
} from '../../assets/styles/components/articles';

import ArticlesCards from './ArticlesCards';

import api from '../../services/api';


function Articles() {

  const [articles, setArticles] = useState([]);

  async function getArticles() {
    try {
      const { data } = await api.get('api/articles')
      setArticles(data)
    } catch (error) {
      alert('Ocorreu um erro ao buscar os items')
    }
  }
  
  useEffect(() => {
    getArticles()
  }, [])

  return (
      <ArticleContainer>
        {articles.map((item) => {
            return <ArticlesCards key={item.id} props={item} />
          })}
      </ArticleContainer>
  );
}

export default Articles;
