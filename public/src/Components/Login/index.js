import React, { useState, useEffect } from 'react'

import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';

import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import {
  LoginContainer,
  LoginInputNBtnContainer,
  LoginInputContainer,
  LoginBtn,
  LoginBtnAside,
  LoginBtnText,
  LoginBtnTextAside,
  LoginFgtText,
  LoginFgtTextLink,
  LoginText,
  ContainerInput,
  AsideContainer,
  MainContainer,
  LoginTextAside,
  LoginTextAuxAside,
  RegisterBtnText
} from '../../assets/styles/components/login';

import api from '../../services/api';
import color from '../../assets/colors';

function Login() {

  const [open, setOpen] = useState(false);
  const [email1, setEmail1] = useState('');
  const [userLogin, setUserLogin] = useState({
    email: '', 
    password: ''
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const loginAuth = async (userToLogin) => {
    try {
      const user = await requestLogin(userToLogin);

      localStorage.setItem("token", user.token);

      history.back();
    } catch (error) {
      alert(error);
    }
    }
  
  async function requestLogin(login) {
    try {
      const { data } = await api.post('api/users/login', login)
      return data
    } catch (error) {
      alert(error)
    }
  }

  return (

    <MainContainer>
      <AsideContainer>
        <LoginTextAside>Criar Conta</LoginTextAside>
        <LoginTextAuxAside>
          Venha fazer parte desse projeto que ajudará você a ter um modo de vida mais saudável!
        </LoginTextAuxAside>
        <LoginBtnAside>
            <LoginBtnTextAside href="Register">Registre-se</LoginBtnTextAside>
        </LoginBtnAside>
      </AsideContainer>
      <LoginContainer>
        <LoginInputNBtnContainer>
          <LoginInputContainer>
            <LoginText>Bem vindo(a)</LoginText>
            <ContainerInput>
              <EmailIcon style={{ height: '15px', weigth: '15px', marginRight: '10px' }} />
              <TextField 
               id="email"
               label="Email"
               type="email"
               variant="outlined"
               value={userLogin.email}
               onChange = {(e) => {
                setUserLogin(prevUserLogin => {
                  return { ...prevUserLogin, email: e.target.value }
                  })
                }} 
              />
            </ContainerInput>
            <ContainerInput>
              <LockIcon style={{ height: '15px', weigth: '15px', marginRight: '10px', alignItems: 'center' }} />
              <TextField
                id="password"
                label="Password"
                type="password"
                variant="outlined"
                value={userLogin.password}
                onChange = {(e) => {
                  setUserLogin(prevUserLogin => {
                    return { ...prevUserLogin, password: e.target.value }
                  })
                }}
              />
              
            </ContainerInput>
            <LoginFgtTextLink 
              style={{ cursor: 'pointer' }}
              onClick={handleClickOpen}
            >
              Esqueci minha senha
            </LoginFgtTextLink>
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle style={{ display: 'flex', justifyContent: 'center', color: `${color.polishedPine}`}} id="alert-dialog-title">{"Esqueci a senha"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                 Informe seu email para recebimento de um link para alteração de sua senha:
                </DialogContentText>
              </DialogContent>
              <TextField 
               style={{ width: '600px', alignItems: 'center', justifyContent: 'center' }}
               id="email"
               type="email"
               variant="outlined"
               value={email1}
               onChange = {e => setEmail1(e.target.value)} 
              />
              <DialogActions>
                <Button style={{ color: `${color.polishedPine}` }} onClick={handleClose} color="primary" autoFocus>
                  Cancelar
                </Button>
                <Button  style={{ color: `${color.polishedPine}` }} onClick={handleClose} color="primary" autoFocus>
                  Enviar
                </Button>
              </DialogActions>
            </Dialog>

            <LoginBtn 
              onClick={() => loginAuth(userLogin)}  
            >
              <LoginBtnText>
                  Entrar 
              </LoginBtnText>
            </LoginBtn>
            <LoginFgtText>
              Você não tem conta ainda?
              <RegisterBtnText 
              href="Register" 
              >
              Registrar
            </RegisterBtnText> 
            </LoginFgtText>
          </LoginInputContainer>
        </LoginInputNBtnContainer>
      </LoginContainer>
    </MainContainer>
  )
}

export default Login
