import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import TextField from '@material-ui/core/TextField';

import {
  AccountContainer,
  FormContainer,
  PreviewContainer,
  ContainerInput,
  InputText,
  CardContacts,
  CardImgContact,
  CardContainer,
  ContainerImage,
  AccountBtnText,
  AccountBtn,
  SelectField,
  FieldsContainer
} from '../../assets/styles/components/account';

import color from '../../assets/colors';
import images from '../../assets/images';

import api from '../../services/api';

const useStyles = makeStyles(() => ({
  root: {
    background: `${color.magnolia}`,
    color: `${color.azulEscuro}`,
    width: '300px',
    borderRadius: '20px',
    boxShadow: `${'1px 2px 5px black'}`,
    margin: '40px 20px'
  },
  header: {
    background: `${color.magnolia}`,
    color: `${color.azulEscuro}`,
    display: 'flex',
    alignItems: 'center',
    marginBottom: '20px',
    justifyContent: 'left',
    height:'50px',
    fontWeight: 'bold'
  },
  img: {
    height: '300px',
    maxWidth: 400,
    overflow: 'hidden',
    display: 'block',
    width: '100%',
    padding: '10px'
  }
}));


export default function Account() {
  
  const token = localStorage.getItem('token');

  const [localizations, setLocalizations] = useState([]);
  const [personal, setPersonal] = useState({
    name: '',
    contact: '',
    speciality: '',
    insta: '',
    description: '',
    local: '',
    image: ''
  });

  const getLocations = async () => {
    try {
      const response = await api.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados/PE/municipios');

      setLocalizations(response.data); 
    } catch (error) {
      alert(error);
    }
  }

  useEffect(() => {
    getLocations();
  }, [])

  const history = useHistory();
  const createPersonal = async (personalData) => {
    try {
    const personalId = await api.post("/api/personal/create", personalData, {
      headers: {
        'Authorization': `Bearer ${token}`
      },
    }); 
      localStorage.setItem('id', personalId);
      history.push('/');
    } catch (error) {
     alert(error);
    }
  } 

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (  
    <AccountContainer>
      <FormContainer>
          <FieldsContainer>
            <ContainerInput>
              <InputText>Nome de usuário</InputText>
              <TextField
                size="small"
                style= {{ width: '250px'}}
                id="name"
                label="Nome"
                placeholder="Digite seu nome"
                variant="outlined"
                value={personal.name}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, name: e.target.value }
                  })
                }}
              />
            </ContainerInput>
            <ContainerInput>
              <InputText>Localidade</InputText>
              <FormControl variant="filled">
                <SelectField
                  labelId="Escolha seu município"
                  id="local"
                  value={personal.local}
                  onChange = {(e) => {
                    setPersonal(prevPersonal => {
                      return { ...prevPersonal, local: e.target.value }
                    })
                  }}
                >
                  {localizations.map(municipio => {
                    return <MenuItem key={municipio.id} value={municipio.nome}>{municipio.nome}</MenuItem>
                  })} 
                </SelectField>
              </FormControl>
            </ContainerInput>
            <ContainerInput>
              <InputText>Descrição</InputText>
              <TextareaAutosize 
                rowsMax={4}
                style= {{ width: '250px', height: '40px' }}
                id="description"
                placeholder="Digite uma descrição"
                label="descricao"
                value={personal.description}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, description: e.target.value }
                  })
                }}
              />
            </ContainerInput>
            <ContainerInput>
              <InputText>Telefone</InputText>
              <TextField
                size="small"
                style= {{ width: '250px'}}
                type="tel"
                id="contact"
                placeholder="Digite seu telefone de contato"
                label="Telefone"
                variant="outlined"
                value={personal.contact}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, contact: e.target.value }
                  })
                }}
              />
            </ContainerInput>
            <ContainerInput>
              <InputText>Instagram</InputText>
              <TextField
                size="small"
                style= {{ width: '250px'}}
                id="insta"
                placeholder="Digite sua conta de instagram"
                label="Instagram"
                variant="outlined"
                value={personal.insta}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, insta: e.target.value }
                  })
                }}
              />
            </ContainerInput>
            <ContainerInput>
              <InputText>Especialidade</InputText>
              <TextField
                size="small"
                style= {{ width: '250px'}}
                id="speciality"
                placeholder="Digite sua especialidade"
                label="Especialidade"
                variant="outlined"
                value={personal.speciality}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, speciality: e.target.value }
                  })
                }}
              />
            </ContainerInput>
            <ContainerImage>
              <InputText>Imagem</InputText>
              <TextField
                size="small"
                style= {{ width: '250px'}}
                id="image"
                variant="outlined"
                type="text"
                value={personal.image}
                onChange = {(e) => {
                  setPersonal(prevPersonal => {
                    return { ...prevPersonal, image: e.target.value }
                  })
                }}
              />
            </ContainerImage>
          </FieldsContainer> 
          <AccountBtn onClick={() => createPersonal(personal)}>
            <AccountBtnText>
                Salvar 
            </AccountBtnText>
          </AccountBtn>
      </FormContainer>
      <PreviewContainer>
        <CardContainer>
          <Card className={classes.root}>
            <CardHeader
              className={classes.header}
              avatar={personal.image ? <Avatar src={personal.image} /> : <Avatar>{personal.name[0]}</Avatar>}
              title={personal.name}
              subheader={personal.local ? `${personal.local}`+'/PE' : ""}
            />
            <CardActionArea className={classes.img}>
              <CardMedia
                component="img"
                alt="perfil"
                height="200"
                image={personal.image ? personal.image : images.logo}
                title="image"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  {personal.speciality}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p" style={{ wordWrap: 'break-word' }}>
                  {personal.description}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardContacts>
              <CardImgContact src={images.personal.contacts.whatsapp} />
              <CardImgContact src={images.personal.contacts.instagram} />
              <CardImgContact src={images.personal.contacts.email} />
            </CardContacts>
          </Card>
        </CardContainer>
      </PreviewContainer>
    </AccountContainer>
  );
}