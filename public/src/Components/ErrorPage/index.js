import React from 'react';

import { 
    MainContainer,
    Img,
    Text,
    MainText
} from '../../assets/styles/components/errorPage';

import images from '../../assets/images'

export default function ErrorPage() {

    const number_images = images.error.length;
    const index = Math.floor((Math.random() * (number_images)))

  return (
    
    <MainContainer>
        <MainText>Oops!</MainText>
        <Text>Página não encontrada! :(</Text>
        <Img src={images.error[index]} />
    </MainContainer>
  )
}