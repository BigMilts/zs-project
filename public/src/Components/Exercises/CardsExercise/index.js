import React, { useState, useEffect } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import clsx from 'clsx';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Switch from '@material-ui/core/Switch';

import colors from '../../../assets/colors';

import Typography, { ExercisesCardsContainer } from '../../../assets/styles/components/exercises';

const useStyles = makeStyles((theme) => ({
  root: {
    color: `${colors.secondary}`,
    maxWidth: 400,
    flexGrow: 1,
    padding: '15px',
    borderRadius: '10px',
  },
  root1: {
    background: `${colors.secondary}`,
    color: `${colors.primary}`,
    maxWidth: 400,
    flexGrow: 1,
    padding: '15px',
    borderRadius: '10px',
  },
  header: {
    color: `${colors.secondary}`,
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  header1: {
    color: `${colors.primary}`,
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  img: {
    height: 200,
    maxWidth: 400,
    overflow: 'hidden',
    display: 'block',
    width: '100%',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}))

export default function ExercisesCard(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [expanded, setExpanded] = useState(false);
  const [activeStep, setActiveStep] = useState(0);
  const [state, setState] = useState({
    checkedA: true,
    checkedB: false,
  });

  const data = props.props

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };


  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  return (
    <ExercisesCardsContainer>
      <Card className={state.checkedB ? classes.root1 : classes.root}>
        <CardHeader
          className={state.checkedB ? classes.header1 : classes.header}
          avatar={<Avatar aria-label="recipe">{data.group}</Avatar>}
          title={data.name}
        />
        <img className={classes.img} src={data.imgUrl} alt={data.name} />
        <CardActions disableSpacing>
          <Switch
            size="small"
            checked={state.checkedB}
            onChange={handleChange}
            color="primary"
            name="checkedB"
            inputProps={{ 'aria-label': 'primary checkbox' }}
          />
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            {state.checkedB
              ?
              <ExpandMoreIcon style={{ color: `${colors.primary}` }} />
              :
              <ExpandMoreIcon style={{ color: `${colors.secondary}` }} />
            }
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          {state.checkedB
            ?
            <CardContent style={{ background: `${colors.secondary}`, color: `${colors.primary}` }} >
              <Typography paragraph>{data.member}</Typography>
              <Typography paragraph>Grupo {data.group}</Typography>
              <Typography paragraph>{data.description}</Typography>
            </CardContent>
            :
            <CardContent style={{ background: `${colors.primary}`, color: `${colors.secondary}` }} >
              <Typography paragraph>{data.member}</Typography>
              <Typography paragraph>Grupo {data.group}</Typography>
              <Typography paragraph>{data.description}</Typography>
            </CardContent>
          }
        </Collapse>
      </Card>
    </ExercisesCardsContainer>
  )
}
