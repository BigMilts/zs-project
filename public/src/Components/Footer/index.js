import React from 'react';

import MailOutlineIcon from '@material-ui/icons/MailOutline';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Tooltip from '@material-ui/core/Tooltip';

import {
  FooterContainer,
  FooterContainerContact,
  FooterContainerContactNet,
  FooterContainerContactText,
  FooterContainerContactImg,
  ContainerContact,
  ContainerLogo,
  FooterLogoImg,
  FooterLogoText,
  FooterText,
  Container
} from '../../assets/styles/components/footer';

import images from '../../assets/images';

function Footer() {
  return (
    
    <FooterContainer>
      <ContainerLogo>
          <FooterLogoImg src={images.logo} />
        <FooterLogoText>
          ZS Fitness
        </FooterLogoText>
        <FooterText>
          Nosso somos uma plataforma Open Source que tem o objetivo de proporcionar
          mais saúde às pessoas através de exercícios com auxílio de
          treinadores qualificados!
        </FooterText>
      </ContainerLogo>

      <ContainerContact>
        <FooterContainerContactNet>
          <FooterContainerContactImg src={images.footer.twitterLogo} />
          <FooterContainerContactText fontSize="9px">Twitter</FooterContainerContactText>
        </FooterContainerContactNet>

        <FooterContainerContactNet>
          <FooterContainerContactImg src={images.footer.instaLogo} />
          <FooterContainerContactText fontSize="9px">Instagram</FooterContainerContactText>
        </FooterContainerContactNet>

        <FooterContainerContactNet>
          <FooterContainerContactImg src={images.footer.faceLogo} />
            <FooterContainerContactText fontSize="9px">Facebook</FooterContainerContactText>
        </FooterContainerContactNet>
      </ContainerContact>

      <FooterContainerContact>
        <Container>
          <MailOutlineIcon style={{ height: '18px' }} color="primary" variant="true" />
          <FooterContainerContactText>zsfitness@gmail.com</FooterContainerContactText>
        <Container>
        </Container>
          <ContactPhoneIcon style={{ height: '16px' }} color="primary" variant="true" />
          <FooterContainerContactText>(81) 9999-9999</FooterContainerContactText>
        </Container>
        <Container>
          <Tooltip title="Nós agradecemos demais qualquer tipo de ajuda! :)">
            <MonetizationOnIcon style={{ height: '16px' }} color="primary" variant="true" />
          </Tooltip>
          <FooterContainerContactText>Pix: zsfitness@gmail.com</FooterContainerContactText>
        </Container>
      </FooterContainerContact>
    </FooterContainer>
  );
}

export default Footer;
