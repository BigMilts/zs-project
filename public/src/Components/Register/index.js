import React, { useState } from 'react';

import {
  RegisterContainer,
  RegisterInputNBtnContainer,
  RegisterInputContainer,
  RegisterBtn,
  RegisterBtnText,
  RegisterText,
  ContainerInput,
} from '../../assets/styles/components/register';

import TextField from '@material-ui/core/TextField';

import api from '../../services/api';

import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';

function Register() {
  const [userRegister, setUserRegister] = useState({
      email: '',
      password: ''
  });

  async function requestRegister(register) {
    try {
      const { data } = await api.post('api/users/create', register)
      return data
    } catch (error) {
      alert(error)
    }
  }
 
  return (
    <RegisterContainer>
      <RegisterInputNBtnContainer>
        <RegisterInputContainer>
          <RegisterText>Criar conta</RegisterText>
          <ContainerInput>
              <EmailIcon style={{ height: '15px', weigth: '15px', marginRight: '10px' }} />
              <TextField 
               id="email"
               label="Email"
               type="email"
               variant="outlined"
               value={userRegister.email}
               onChange = {(e) => {
                setUserRegister(prevUserLogin => {
                  return { ...prevUserLogin, email: e.target.value }
                  })
                }} /> 
            </ContainerInput>
            <ContainerInput>
              <LockIcon style={{ height: '15px', weigth: '15px', marginRight: '10px', alignItems: 'center' }} />
              <TextField
                id="password"
                label="Password"
                type="password"
                variant="outlined"
                value={userRegister.password}
                onChange = {(e) => {
                  setUserRegister(prevUserLogin => {
                    return { ...prevUserLogin, password: e.target.value }
                    })
                  }} 
              />
            </ContainerInput>
        </RegisterInputContainer>
        <RegisterBtn onClick={() => requestRegister(userRegister)}>
          <RegisterBtnText href="/">Cadastrar</RegisterBtnText>
        </RegisterBtn>
      </RegisterInputNBtnContainer>
    </RegisterContainer>
  );
}

export default Register;
