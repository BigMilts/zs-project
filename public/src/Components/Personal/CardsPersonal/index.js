import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import images from '../../../assets/images';
import color from '../../../assets/colors';

import {
  ContainerCard,
  CardImgContact,
  CardContacts,
  CardLink
} from '../../../assets/styles/components/personal';

const useStyles = makeStyles((theme) => ({
  root: {
    background: `${color.magnolia}`,
    color: `${color.azulEscuro}`,
    maxWidth: 400,
    borderRadius: '10px',
    boxShadow: `${'1px 2px 5px black'}`,
    margin: '40px 0'
  },
  header: {
    background: `${color.magnolia}`,
    color: `${color.azulEscuro}`,
    display: 'flex',
    alignItems: 'center',
    marginBottom: '20px',
    justifyContent: 'left',
    height:'50px',
    fontWeight: 'bold'
  },
  img: {
    height: '400px',
    maxWidth: 400,
    overflow: 'hidden',
    display: 'block',
    width: '100%',
    padding: '10px'
  }
}));

export default function ImgMediaCard(props) {

  const data = props.props;
  const classes = useStyles();

  return (
    <ContainerCard>
      <Card className={classes.root}>
        <CardHeader
          className={classes.header}
          avatar={
            <Avatar src={data.image ? data.image : data.name[0].toUpperCase()} aria-label="recipe" />
          }
          title={data.name}
          subheader={`${data.local}/PE`}
        />
        <CardActionArea className={classes.img}>
          <CardMedia
            component="img"
            alt="perfil"
            height="200"
            image={data.image ? data.image : images.logo}
            title={data.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {data.speciality}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {data.description}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardContacts>
          <CardLink href={`https://wa.me/5581${data.contact}`} target="_blank">
              <CardImgContact src={images.personal.contacts.whatsapp} />
          </CardLink>
          <CardLink href={`https://www.instagram.com/${data.insta}`} target="_blank">
              <CardImgContact src={images.personal.contacts.instagram} />
          </CardLink>
          <CardLink href={`mailto:${data.email}?subject=ZS%20fitness`} target="_blank">
              <CardImgContact src={images.personal.contacts.email} />
          </CardLink>
        </CardContacts>
      </Card>
    </ContainerCard>
  );
}
