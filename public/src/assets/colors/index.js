
const colors = {
  blue: '#36558F',
  greenLight: '#539987',
  primary: '#FFFAFF',
  greenDark: '#122C34',
  greenNormal: '#218380',
  green: '#539987',
  secondary: '#040F16',
  aux: '#BB6B00',
};

export default colors;
