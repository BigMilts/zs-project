/* eslint-disable global-require */

const images = {
  logo: require('./header/logo.png').default,
  homeImage: {
    image1: require('./home/img16.jpg').default,
    image2: require('./home/img4.jpg').default,
    image3: require('./home/img1.jpg').default,
    image4: require('./home/img15.jpg').default,
    image5: require('./home/img21.jpg').default,
    perfil: {
      image1: require('./home/img18.jpg').default,
      image2: require('./home/img19.jpg').default,
      image3: require('./home/img20.jpg').default,
    },
  },
  footer: {
    faceLogo: require('./footer/facebook.png').default,
    instaLogo: require('./footer/instagram.png').default,
    emailLogo: require('./footer/email-logo.png').default,
    twitterLogo: require('./footer/twitter.png').default,
    helpUs: require('./footer/img8.jpg').default,
  },
  personal: {
    contacts: {
      whatsapp: require('./personal/whatsapp.png').default,
      instagram: require('./personal/instagram.png').default,
      email: require('./personal/email.png').default,
    }
  },
  error: [
    require('./error/image1.jpg').default,
    require('./error/image2.jpg').default,
    require('./error/image3.jpg').default,
    require('./error/image4.jpg').default,
    require('./error/image5.jpg').default,
    require('./error/image6.jpg').default,
  ],
}

export default images;
