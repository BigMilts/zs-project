import styled from 'styled-components';

import colors from '../../../colors';

export const RegisterContainer = styled.div`
  height: 100%;
  width: 100%;
  background-color: ${colors.primary};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const RegisterInputNBtnContainer = styled.div`
  height: 77%;
  width: 35%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  box-shadow: 1px 2px 2px black;
`;

export const RegisterText = styled.p`
  margin-bottom: 40px; 
  font-size: 35px;
  color: ${colors.secondary};
  font-weight: bold;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const RegisterInputContainer = styled.div`
  height: 50%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
`;

export const RegisterInput = styled.input`
  height: 12%;
  width: 50%;
  border: 1px solid black;
  border-radius: 12px;
  margin-top: 20px;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    border: 1px solid ${colors.blue};
    box-shadow: 0 3px 3px rgba(0, 0, 0, .15);
  }
`;

export const RegisterInputRadioTemp = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  margin-top: 10px;
`;

export const ContainerInput = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;

export const RegisterBtn = styled.button`
  width: 250px;
  background-color: black;
  margin-top: 20px;
  padding: 10px;
  border: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 20px;
  transition: .5s;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    background: ${colors.blue};
  }
`;

export const RegisterBtnText = styled.a`
  font-size: 17px;
  color: ${colors.primary};
  font-weight: bold;
  text-decoration: none;
`;
