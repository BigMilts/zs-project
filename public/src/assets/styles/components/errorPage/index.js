import styled from 'styled-components';

import colors from '../../../colors'

export const MainContainer = styled.div`
  height: 100%; 
  width: 100%; 
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const MainText = styled.p`
    margin-bottom: 10px;
    font-size: 3.2rem; 
    font-weight: bold;
    color: ${colors.aux}
`;

export const Text = styled.p`
    margin-bottom: 30px;
    font-size: 1.8rem; 
`;

export const Img = styled.img`
  height: 300px;
  width: 300px;
  border-radius: 200px;
  box-shadow: 1px 2px 3px ${colors.secondary};
`;
