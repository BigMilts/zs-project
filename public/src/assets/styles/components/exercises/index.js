import styled from 'styled-components'

import color from '../../../colors'

import Typography from '@material-ui/core/Typography'

export const ExercisesCardsContainer = styled.div`
  width: 300px;
  margin: 50px;
`
export const ExerciseContainer = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: auto auto auto;
  justify-content: space-around;
  overflow: auto;
`
export default Text = styled(Typography)`
  text-align: center;
  justify-content: center;
  font-size: 5px;
`

export const SearchContainer = styled.div`
  width: 100%;
  display: flex;
  flex: 1;
  justify-content: center;
  margin: 20px 0 10px 0;
  border-radius: 20px;
`
