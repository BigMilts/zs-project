import styled from 'styled-components';

import colors from '../../../colors';

export const LoginContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ContainerInput = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;

export const LoginText = styled.p`
  margin-bottom: 50px; 
  font-size: 28px;
  color: ${colors.secondary};
  font-weight: bold;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoginInputNBtnContainer = styled.div`
  height: 85%;
  width: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  box-shadow: 1px 2px 3px black;
`;

export const LoginInputContainer = styled.div`
  height: 100%;
  width: 70%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  `;

export const LoginInput = styled.input`
  height: 5%;
  border-radius: 12px;
  width: 60%;
  border: 1px solid black;
  margin-top: 10px;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    border: 1px solid ${colors.blue};
    box-shadow: 0 3px 3px rgba(0, 0, 0, .15);
  } 
`;

export const LoginBtn = styled.button`
  width: 250px;
  background-color: black;
  margin-top: 25px;
  padding: 10px;
  border: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 20px;
  transition: .5s;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    background: ${colors.blue};
  }
`;

export const MainContainer = styled.div`
  height: 100vh;
  background-color: ${colors.primary};
  display: flex;
  flex-direction: row;
`;

export const AsideContainer = styled.div`
  background-color: ${colors.blue};
  width: 70%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  animation: go-back 2s;
  
  @keyframes go-back {
  from {
    transform: translateX(-300px);
  }
  to {
    transform: translateX(0);
  }
}
`;

export const LoginBtnText = styled.a`
  font-size: 15px;
  color: ${colors.primary};
  font-weight: bold;
  text-decoration: none;
  cursor: pointer;
`;

export const RegisterBtnText = styled.a`
  font-size: 10px;
  color: black;
  font-weight: bold;
  text-decoration: none;
  margin-left: 3px;

  :hover {
    color: ${colors.blue}
  }
`;

export const LoginBtnTextAside = styled.a`
  font-size: 10px;
  color: ${colors.secondary};
  text-decoration: none;
  text-align: center;
  justify-content: center;

  :hover {
    color: ${colors.primary};
    font-weight: bold;
  }
`;

export const LoginFgtTextLink = styled.a`
    text-decoration: none;
    font-size: 10px;
    color: black;

    :hover {
      color: ${colors.secondary};
    }
`;

export const LoginFgtText = styled.p`
    font-size: 10px;
    color: black;
    margin-top: 10px;
`;

export const LoginTextAside = styled.p`
  margin-bottom: 30px; 
  font-size: 35px;
  color: ${colors.secondary};
  font-weight: bold;
`;
export const LoginTextAuxAside = styled.p`
  margin-bottom: 30px; 
  font-size: 12px;
  width: 80%;
  color: ${colors.primary};
`;

export const LoginBtnAside = styled.button`
  width: 150px;
  height: 20px;
  background-color: ${colors.primary};
  padding: 20px;
  border: 0;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    background-color: ${colors.greenNormal};
  }
`;
