import styled from 'styled-components'
import { TextField, InputAdornment } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search'

import colors from '../../../colors';

export const InputField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: `${colors.greenLight}`,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: `${colors.greenLight}`,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: `${colors.greenLight}`,
      },
      '&:hover fieldset': {
        borderColor: `${colors.greenLight}`,
      },
      '&.Mui-focused fieldset': {
        borderColor: `${colors.greenLight}`,
      },
    },
    width: '30%',
    height: '10%'
  },
})(TextField)

export const SearchField = styled((props) => (
  <InputField
    variant="outlined"
    type="search"
    placeholder={props.info ? `Pesquise o ${props.info}` : 'Pesquisar'}
    InputProps={{
      startAdornment: (
        <InputAdornment position="start">
          <SearchIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))`
  // width: 30%;
`
