import styled from 'styled-components';

import colors from '../../../colors';

import Select from '@material-ui/core/Select';

export const AccountContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`;

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 80%;
  width: 55%;
  margin: auto;
  box-shadow: 1px 2px 3px black;
`;

export const FieldsContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto;

  @media screen and (max-width: 1100px) {
    grid-template-columns: auto;
    overflow-y: scroll;
  }
`;

export const AccountBtn = styled.button`
  width: 300px;
  background-color: ${colors.secondary};
  padding: 8px;
  border-radius: 20px;
  transition: .5s;
  margin: 30px auto;
  outline: 0px auto -webkit-focus-ring-color;
  border: 1px solid ${colors.secondary};

  :hover {
    background: ${colors.verdeEscuro1};
    border: 1px solid ${colors.greenDark};
  }
`;

export const AccountBtnText = styled.a`
  font-size: 15px;
  color: ${colors.primary};
  font-weight: bold;
  text-decoration: none;
  cursor: pointer;
`;

export const ContainerInput = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px;
`;

export const ContainerImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const InputText = styled.p`
  display: flex;
  margin: 10px; 
  font-size: 10px;
  color: ${colors.secondary};
  font-weight: bold;
`;


export const PreviewContainer = styled.div`
  height: 80%;
  width: 40%;
  margin: auto 20px;
  box-shadow: 1px 2px 3px black;
  background: ${colors.secondary};
  border-radius: 30px;
`;

export const CardContainer = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
  box-shadow: 10px 10px 10px ${colors.primary};
`;

export const CardContacts = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const CardImgContact = styled.img`
  height: 1.2rem;
  margin: 0.6rem;
  text-align: center;
  justify-content: left;
  cursor: pointer;
`;

export const CardLink = styled.a`
`;

export const SelectField = styled(Select)`
  display: flex;
  width: 250px;
  height: 40px;
`;

export const TextContactField = styled.input`
  display: flex;
  width: 250px;
  height: 40px;
`;









