import styled from 'styled-components';
import { FaBars, FaMaxcdn } from 'react-icons/fa';
import { BsBackspace } from 'react-icons/bs';

import colors from '../../../colors';

import { Link } from 'react-router-dom';

export const HeaderMain = styled.header`
  display: grid;
  grid-template-columns: auto auto auto;
  justify-content: space-around;
  align-items: center;
  height: 75px;
  background:${colors.secondary};
  box-shadow: 5px 0 10px ${colors.secondary};
`;

export const ContainerLogo = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 1rem;

  animation: go-back 3s;
  
  @keyframes go-back {
  from {
    transform: translateX(300px);
  }
  to {
    transform: translateX(0);
  }
}
`;

export const ContainerImgLogo = styled.a`
`;

export const HeaderLogoImg = styled.img`
  height: 3.5em;
  width: 3.5em; 
`;

export const HeaderLogoText = styled.p`
  display: flex;
  align-items: center;
  justify-content: right; 
  font-size: 1.5rem;
  margin-left: 25px;
  font-weight: bold;
  color: ${colors.aux};
  font-family: 'Raleway', sans-serif;
`;

export const HeaderBtnsLogin = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-right: 2rem;

  animation: go-back 3s;
  
  @keyframes go-back {
  from {
    transform: translateX(300px);
  }
  to {
    transform: translateX(0);
  }
}
`;

export const NavList = styled.li`
  list-style: none;
  margin: 0px 30px;
  `;

  export const ListLinks = styled.ul`
    display: flex;
    flex-direction: row;
  `;

export const NavMain = styled.nav`
 animation: go-back 2s;
  
  @keyframes go-back {
  from {
    transform: translateX(300px);
  }
  to {
    transform: translateX(0);
  }
}
`;

export const ButtonMobile = styled.div`
`;

export const LinkMenu = styled(Link) `
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${colors.primary};
  text-decoration: none;
  border-radius: 8px;
  padding: 0.6rem ;
  font-size: 13px;
  
  :hover {
    transition: all .2s ease-out;  
  }
`;

export const ButtonHeader = styled.button`
  display: flex;
  justify-content: center;
  padding: 10px 20px;
  background-color: ${colors.blue};  
  
  border: 0;
  border-radius: 40px;
  cursor: pointer;
  transition: all .3s ease-out;

  outline: 0px auto -webkit-focus-ring-color;

  :hover {
    background-color: ${colors.aux};
  }

  @media screen and (max-width: 768px) {
    display: none; 
  }
`;

export const LinkButton = styled.a`
  text-decoration: none;
  color: ${colors.primary};
  transition: all .3s ease-out;
  font-size: 10px;
`;

export const Bars = styled(FaBars)`
  display: none;
  color: #fff;

  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const CancelButton = styled(BsBackspace)`
  display: none;
  color: ${colors.primary};

  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const LinkPerfil = styled.a`
  text-decoration: none;
  color: ${colors.secondary};
`;
