# Executando o front-end
 
## Configuração do ambiente:

- [Node versão 14](https://nodejs.org/en/)

## Yarn versão 14:

    npm install --global yarn

## Instalando as dependências do projeto

    cd public
    yarn 

## Rodando o projeto
    yarn start