import { Controller, Get, Middleware, Post } from '@overnightjs/core';
import { StatusCodes } from 'http-status-codes';
import { PersonalCreateDTO } from '@src/dto/personal/PersonalCreateDTO';
import express from 'express';
import personalService from '@src/services/PersonalService';

import personalMiddleware from '@src/middleware/PersonalRequestMiddleware';
import userRequestMiddleware from '@src/middleware/UserRequestMiddleware';
import Personal from '@src/models/Personal';

@Controller('api/personal')
export class PersonalController {
    @Get('')
    public async getAllPersonals(request: express.Request, response: express.Response) {
        try {
            const personals = await personalService.getAllPersonals();
            response.send(personals);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }
    @Get('single/:id')
    public async getPersonal(request: express.Request, response: express.Response) {
        try {
            const { id } = request.params;
            const personal = await personalService.getPersonal(id);
            response.send(personal);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }

    @Post('create')
    @Middleware([
        personalMiddleware.validateRequest,
        userRequestMiddleware.isAuthorized,
        personalMiddleware.validateSamePersonalDoesntExist,
    ])
    public async createPersonal(request: express.Request, response: express.Response) {
        try {       
            const personalDTO: PersonalCreateDTO = request.body;
            const personal: Personal = await personalService.createPersonal(personalDTO);
            response.status(StatusCodes.OK).send(personal.id);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }

    @Get('top3')
    public async getTop3Personals(request: express.Request, response: express.Response) {
        try {
            const personals = await personalService.getTop3Personals();
            response.send(personals);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }
}
