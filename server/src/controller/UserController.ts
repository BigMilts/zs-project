import express from 'express';
import { StatusCodes } from 'http-status-codes';
import { Controller, Middleware, Post } from '@overnightjs/core';
import { UserLoginDTO } from '@src/dto/user/UserLoginDTO';
import { UserLoggedDTO } from '@src/dto/user/UserLoggedDTO';
import { CreateUserDTO } from '@src/dto/user/CreateUserDTO';
import userRequestMiddleware from '@src/middleware/UserRequestMiddleware';
import userService from '@src/services/UsersService';

@Controller('api/users')
export class UsersController {
    @Post('create')
    @Middleware([userRequestMiddleware.validateRequiredUserBodyFields, userRequestMiddleware.validateSameEmailDoesntExist])
    public async createUser(request: express.Request, response: express.Response) {
        try {
            const userDTO: CreateUserDTO = request.body;
            await userService.createUser(userDTO);
            response.status(StatusCodes.CREATED);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }

    @Post('login')
    @Middleware([userRequestMiddleware.validateRequiredUserBodyFields, userRequestMiddleware.validateUserExists])
    public async loginUser(request: express.Request, response: express.Response) {
        try {
            const userDto: UserLoginDTO = request.body;
            const userLogged: UserLoggedDTO = await userService.loginUser(userDto);
            response.status(StatusCodes.OK).send(userLogged);
        } catch (exception) {
            response.status(StatusCodes.INTERNAL_SERVER_ERROR).send(exception);
        }
    }

    @Post('professor')
    @Middleware([
        userRequestMiddleware.validateUserExists,
        userRequestMiddleware.isAuthorized,
        userRequestMiddleware.isProfessor,
    ])
    public async isProfessor(request: express.Request, response: express.Response) {
        response.status(StatusCodes.OK).send('Is a professor');
    }
}
