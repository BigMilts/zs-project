import { Controller, Get } from '@overnightjs/core';
import express from 'express';
import exercicesService from '@src/services/ExercicesService';

@Controller('api/exercices')
export class ExercicesController {
    @Get('')
    public getExercices(request: express.Request, response: express.Response): void {
        response.send(exercicesService.getExercices());
    }
}
