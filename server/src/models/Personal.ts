import { Column, Entity, PrimaryColumn } from 'typeorm';
import { v4 as uuid } from 'uuid';

@Entity('personal')
class Personal {
    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    image: string;

    @Column()
    contact: string;

    @Column()
    speciality: string;

    @Column()
    local: string;

    @Column()
    description: string;

    @Column()
    insta: string;

    constructor() {
        if (!this.id) {
            this.id = uuid();
        }
    }
}

export default Personal;
