import { EntityRepository, Repository } from 'typeorm';
import { PersonalCreateDTO } from '@src/dto/personal/PersonalCreateDTO';
import Personal from '@src/models/Personal';

@EntityRepository(Personal)
export class PersonalRepository extends Repository<Personal> {
    async persist(personalDTO: PersonalCreateDTO): Promise<Personal> {
        const personal: Personal = this.create(personalDTO);
        await this.save(personal);
        return personal;
    }
    findByContact(contact: string): Promise<Personal> {
        return this.findOne({ contact });
    }
}
