import { EntityRepository, Repository } from 'typeorm';
import User from '@src/models/User';
import { UserLoginDTO } from '@src/dto/user/UserLoginDTO';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    findByEmail(email: string): Promise<User> {
        return this.findOne({ email });
    }

    async persist(userLogin: UserLoginDTO): Promise<User> {
        const user: User = this.create(userLogin);
        await this.save(user);
        return user;
    }
}
