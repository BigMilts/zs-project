import User from '@src/models/User';
import * as jwt from 'jsonwebtoken';
import userUtils from '@src/utils/UserUtils';
import { CreateUserDTO } from '@src/dto/user/CreateUserDTO';
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '@src/repository/UserRepository';
import { UserLoginDTO } from '@src/dto/user/UserLoginDTO';
import { UserLoggedDTO } from '@src/dto/user/UserLoggedDTO';

class UsersService {
    async createUser(createUserDTO: CreateUserDTO) {
        const userRepository = getCustomRepository(UserRepository);
        await userUtils.encryptPassword(createUserDTO);

        if (createUserDTO.isProfessor === undefined) {
            createUserDTO.isProfessor = false;
        }
        return await userRepository.persist(createUserDTO);
    }

    async loginUser(userDto: UserLoginDTO) {
        const user: User = await this.getUserByEmail(userDto.email);
        const isPasswordCorrect: boolean = await userUtils.isPasswordCorrect(userDto.password, user.password);

        if (isPasswordCorrect) {
            const token = jwt.sign({ userId: user.id }, process.env.SECRET, {
                expiresIn: '1d',
            });
            return new UserLoggedDTO(user.id, user.email, token);
        } else {
            return null;
        }
    }

    async getUserByEmail(email: string) {
        const userRepository = getCustomRepository(UserRepository);
        return await userRepository.findByEmail(email);
    }
}

export default new UsersService();
