import { PersonalCreateDTO } from '@src/dto/personal/PersonalCreateDTO';
import { PersonalRepository } from '@src/repository/PersonalRepository';
import { getCustomRepository } from 'typeorm';

class PersonalService {
    async getAllPersonals() {
        const personalRepository = getCustomRepository(PersonalRepository);
        return await personalRepository.find();
    }

    async getPersonal(id: string) {
        const personalRepository = getCustomRepository(PersonalRepository);
        return await personalRepository.findOne({ id });
    }

    async createPersonal(personalDTO: PersonalCreateDTO) {
        const personalRepository = getCustomRepository(PersonalRepository);
        return await personalRepository.persist(personalDTO);
    }

    async getPersonalByContact(contact: string) {
        const personalRepository = getCustomRepository(PersonalRepository);
        return await personalRepository.findByContact(contact);
    }
    async getTop3Personals() {
        const personalRepository = getCustomRepository(PersonalRepository);
        const personals = await personalRepository.find();
        return personals.slice(0, 3);
    }
}

export default new PersonalService();
