import * as data from '../database/exercises.json';

class ExercicesService {
    getExercices() {
        return data.default;
    }
}
export default new ExercicesService();
