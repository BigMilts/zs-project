export class UserLoggedDTO {
    private id: string;
    private email: string;
    private token: string;

    constructor(id: string, email: string, token: string) {
        this.id = id;
        this.email = email;
        this.token = token;
    }
}
