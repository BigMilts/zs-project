export class CreateUserDTO {
    email: string;
    password: string;
    isProfessor: boolean;
}
