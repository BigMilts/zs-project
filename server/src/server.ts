import './database/index';
import './config/env';
import './config/moduleAlias';

import { Server } from '@overnightjs/core';
import { UsersController } from '@src/controller/UserController';
import { PersonalController } from './controller/PersonalController';
import { ArticlesController } from './controller/ArticlesController';

import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import { ExercicesController } from './controller/ExercicesController';

export class SetupServer extends Server {
    constructor(private port = process.env.PORT) {
        super();
    }

    public async init(): Promise<void> {
        this.setupExpress();
        this.setupControllers();
    }

    private setupExpress(): void {
        this.app.use(bodyParser.json());
        this.app.use(cors());
    }

    private setupControllers(): void {
        const userController = new UsersController();
        const personalController = new PersonalController();
        const exerciceController = new ExercicesController();
        const articlesController = new ArticlesController();
        this.addControllers([userController, personalController, exerciceController, articlesController]);
    }

    public getApp(): express.Application {
        return this.app;
    }

    public start(): void {
        this.app.listen(this.port, () => {
            console.info('Server listening on port: ' + this.port);
        });
    }
}
