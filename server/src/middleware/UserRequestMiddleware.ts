import express from 'express';
import userService from '@src/services/UsersService';
import { StatusCodes } from 'http-status-codes';
import * as jwt from 'jsonwebtoken';
import { getCustomRepository } from 'typeorm';
import { UserRepository } from '@src/repository/UserRepository';
import User from '@src/models/User';
import Joi from 'joi';

const userSchema: Joi.ObjectSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(12).required(),
    isProfessor: Joi.boolean(),
});

class UserRequestMiddleware {
    async validateRequiredUserBodyFields(request: express.Request, response: express.Response, next: express.NextFunction) {
        const { error } = userSchema.validate(request.body);
        if (error) {
            response.status(StatusCodes.BAD_REQUEST).send({ error: error.details.map((x) => x.message).join(', ') });
        } else {
            next();
        }
    }

    async validateSameEmailDoesntExist(request: express.Request, response: express.Response, next: express.NextFunction) {
        const user = await userService.getUserByEmail(request.body.email);
        if (user) {
            response.status(StatusCodes.BAD_REQUEST).send({ error: `User email already exists` });
        } else {
            next();
        }
    }

    async validateUserExists(request: express.Request, response: express.Response, next: express.NextFunction) {
        const user = await userService.getUserByEmail(request.body.email);
        if (user) {
            next();
        } else {
            response.status(StatusCodes.BAD_REQUEST).send({ error: 'This user email does not exists' });
        }
    }

    async isAuthorized(request: express.Request, response: express.Response, next: express.NextFunction) {
        const authHeader = request.headers.authorization;
        let jwtPayload;

        if (!authHeader) {
            response.status(StatusCodes.UNAUTHORIZED).send({ message: 'Token is required' });
        }

        const [, token] = authHeader.split(' ');

        try {
            jwtPayload = await jwt.verify(token, process.env.SECRET);
            response.locals.jwtPayLoad = jwtPayload;
            next();
        } catch {
            response.status(StatusCodes.UNAUTHORIZED).send({ message: 'Token is invalid' });
        }
    }

    async isProfessor(request: express.Request, response: express.Response, next: express.NextFunction) {
        const id = response.locals.jwtPayLoad.userId;
        const userRepository = getCustomRepository(UserRepository);
        let user: User;

        try {
            user = await userRepository.findOneOrFail(id);
        } catch (id) {
            response.status(StatusCodes.BAD_REQUEST).send({ error: 'User not found' });
        }

        if (user.isProfessor) {
            next();
        } else {
            response.status(StatusCodes.UNAUTHORIZED).send({ error: 'User is not a Professor' });
        }
    }
}

export default new UserRequestMiddleware();
